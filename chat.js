
var data_id;
var getMsg;
var ch_id;
var dataChannelName;
var name;
var msgSentBy;
$(document).ready(function () {
    

    Pusher.logToConsole = true;
    var pusher = new Pusher('4ae33aebb71ecb07d5a0', {
        cluster: 'ap2',
        forceTLS: true
    });
  
      var channel = pusher.subscribe('my-channel');
      channel.bind('my-event', function(data) {
        $('.messages').append("<p class = 'sent_message'>" + data['message'] + "</p>")

    });

    
    name = GetParameterValues('uname');  
    var id = GetParameterValues('pwd');  
    function GetParameterValues(param) {  
        var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');  
        for (var i = 0; i < url.length; i++) {  
            var urlparam = url[i].split('=');  
            if (urlparam[0] == param) {  
                return urlparam[1];  
            }  
        }  
    }  
    $('h2').append(name);
   
    channel_func();
});


function channel_func() {
    $.ajax({
        type: 'GET',
        url: "http://localhost:5000/channels/",
        crossDomain: true,
        success: function (res) {
            $(".channel_container").append("<ul>");
            let val = res.resources;
            for (let data of val) {
                data_id = data['channel_id'];
                dataChannelName = data['channel_name'];
                console.log(data_id, dataChannelName)
                appendChannel(data_id, dataChannelName);
            }
            $(".channel_container").append("</ul>")
        }
    });

    
    // post channel 
    $('#submit').click(function () {
        console.log("submit channel");
        let channel_name = { 'channel_name': $('#name').val() };
        addChannel(channel_name);
               
    });


}
function appendChannel(id, name){
    $("ul").append("<li><a id=" + id + " href='#message_container?id=" + id + "' onclick='msgBox(id)'>" + name + "</a></li>");
    $('ul li a').click(function(){
        $('li a').removeClass("active");
        $(this).addClass("active");
    });
}

function addChannel(channel_name){

    console.log("add channel")
    $.ajax({
        type: 'POST',
        url: "http://localhost:5000/channels/",
        data: JSON.stringify(channel_name),
        crossDomain: true,
        ContentType: "application/json",
        success: function (data) {
            $(".channel_container").empty();
            channel_func();

        }

    });
}

function msgBox(id) {
    
    $('.sent_message').empty();
    $("#message_container").show();
    $('.sent-by').empty();
    ch_id = id;
    
    getMsg = function () {
       
        $.ajax({
            type: 'GET',
            url: "http://localhost:5000/message/",
            crossDomain: true,
            success: function (res) {
                let val = res.resources;
                for (let data of val) {
                    if (ch_id == data["channel_id"]) {
                        msgSentBy = data['username'];
                        console.log(msgSentBy);
                        $('.messages').append('<div>');
                        $('.messages').append("<span class = 'sent_message'>" + data['message'] + "</span>");
                        $('.messages').append("<span class = 'sent-by'>Sent by " + msgSentBy + "</span>");
                        $('.messages').append('</div>');
                    }
                }
            }
        });
    }
    getMsg();
}


function saveMessage(text) {
    $.ajax({
        type: 'POST',
        url: "http://localhost:5000/message/",
        data: JSON.stringify(text),
        crossDomain: true,
        contentType: "application/json",

        success: function (data) {
            broadcastMessage(data);
                  
        }
    });
}

function broadcastMessage(text) {
    $.ajax({
        type: 'POST',
        url: "http://localhost:5000/broadcast",
        data: JSON.stringify(text),
        crossDomain: true,
        contentType: "application/json",
        
        success: function (data) {
            alert(data)

        }
    });

}

function sendMessage() {
    console.log(name);
    let message = { 'message': $('#msg').val(), 'channel_id': ch_id, 'username': name};
    console.log(message)
    $('#msg').val('');
    saveMessage(message);
    //console.log("message")
}
